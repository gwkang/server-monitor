var status = require('./report').status();

exports.index = function(req, res){
  res.render('index', { status: status });
};