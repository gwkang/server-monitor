var status = [];
var querystring = require('querystring');

exports.status = function () {
	return status;
};

exports.report = function(req, res){
	var host = querystring.unescape(req.param('hostname'));
	var cpu = querystring.unescape(req.param('cpu'));
	var disk = querystring.unescape(req.param('disk'));
	var mem = querystring.unescape(req.param('mem'));
	var mems = mem.split(' ');
	var st;

	for (var i=0, len=mems.length; i < len; i += 1) {
		mems[i] = parseInt(mems[i] / 1000, 10) + 'k';
	}
	
	var exists = status.some(function (data) {
		if (data.host && data.host === host) {
			data.cpu = cpu;
			data.disk = disk;
			data.mem = mems.join(',');
			data.ts = Date.now();
			st = data;
			return true;
		}
		return false;
	});

	if (!exists) {
		st = {
			host: host,
			cpu: cpu,
			disk: disk,
			mem: mems.join(','),
			ts: Date.now()
		};
		status.push(st);
	}

	console.log(status);

	res.json(200, st);
};